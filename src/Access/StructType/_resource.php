<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for resource StructType
 * Meta information extracted from the WSDL
 * - final: extension restriction
 * @subpackage Structs
 */
class _resource extends AbstractStructBase
{
    /**
     * The comment
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $comment = null;
    /**
     * The externalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * The instructorPosition
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $instructorPosition = null;
    /**
     * The instructorRow
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float|null
     */
    protected ?float $instructorRow = null;
    /**
     * The key
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\CompositeKey|null
     */
    protected ?\Exerp\Access\StructType\CompositeKey $key = null;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The resourceGroupExternalIds
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var string[]
     */
    protected ?array $resourceGroupExternalIds = null;
    /**
     * The seats
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\Seats|null
     */
    protected ?\Exerp\Access\StructType\Seats $seats = null;
    /**
     * Constructor method for resource
     * @uses _resource::setComment()
     * @uses _resource::setExternalId()
     * @uses _resource::setInstructorPosition()
     * @uses _resource::setInstructorRow()
     * @uses _resource::setKey()
     * @uses _resource::setName()
     * @uses _resource::setResourceGroupExternalIds()
     * @uses _resource::setSeats()
     * @param string $comment
     * @param string $externalId
     * @param float $instructorPosition
     * @param float $instructorRow
     * @param \Exerp\Access\StructType\CompositeKey $key
     * @param string $name
     * @param string[] $resourceGroupExternalIds
     * @param \Exerp\Access\StructType\Seats $seats
     */
    public function __construct(?string $comment = null, ?string $externalId = null, ?float $instructorPosition = null, ?float $instructorRow = null, ?\Exerp\Access\StructType\CompositeKey $key = null, ?string $name = null, ?array $resourceGroupExternalIds = null, ?\Exerp\Access\StructType\Seats $seats = null)
    {
        $this
            ->setComment($comment)
            ->setExternalId($externalId)
            ->setInstructorPosition($instructorPosition)
            ->setInstructorRow($instructorRow)
            ->setKey($key)
            ->setName($name)
            ->setResourceGroupExternalIds($resourceGroupExternalIds)
            ->setSeats($seats);
    }
    /**
     * Get comment value
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }
    /**
     * Set comment value
     * @param string $comment
     * @return \Exerp\Access\StructType\_resource
     */
    public function setComment(?string $comment = null): self
    {
        // validation for constraint: string
        if (!is_null($comment) && !is_string($comment)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($comment, true), gettype($comment)), __LINE__);
        }
        $this->comment = $comment;
        
        return $this;
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Exerp\Access\StructType\_resource
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
    /**
     * Get instructorPosition value
     * @return float|null
     */
    public function getInstructorPosition(): ?float
    {
        return $this->instructorPosition;
    }
    /**
     * Set instructorPosition value
     * @param float $instructorPosition
     * @return \Exerp\Access\StructType\_resource
     */
    public function setInstructorPosition(?float $instructorPosition = null): self
    {
        // validation for constraint: float
        if (!is_null($instructorPosition) && !(is_float($instructorPosition) || is_numeric($instructorPosition))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($instructorPosition, true), gettype($instructorPosition)), __LINE__);
        }
        $this->instructorPosition = $instructorPosition;
        
        return $this;
    }
    /**
     * Get instructorRow value
     * @return float|null
     */
    public function getInstructorRow(): ?float
    {
        return $this->instructorRow;
    }
    /**
     * Set instructorRow value
     * @param float $instructorRow
     * @return \Exerp\Access\StructType\_resource
     */
    public function setInstructorRow(?float $instructorRow = null): self
    {
        // validation for constraint: float
        if (!is_null($instructorRow) && !(is_float($instructorRow) || is_numeric($instructorRow))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($instructorRow, true), gettype($instructorRow)), __LINE__);
        }
        $this->instructorRow = $instructorRow;
        
        return $this;
    }
    /**
     * Get key value
     * @return \Exerp\Access\StructType\CompositeKey|null
     */
    public function getKey(): ?\Exerp\Access\StructType\CompositeKey
    {
        return $this->key;
    }
    /**
     * Set key value
     * @param \Exerp\Access\StructType\CompositeKey $key
     * @return \Exerp\Access\StructType\_resource
     */
    public function setKey(?\Exerp\Access\StructType\CompositeKey $key = null): self
    {
        $this->key = $key;
        
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Exerp\Access\StructType\_resource
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get resourceGroupExternalIds value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string[]
     */
    public function getResourceGroupExternalIds(): ?array
    {
        return isset($this->resourceGroupExternalIds) ? $this->resourceGroupExternalIds : null;
    }
    /**
     * This method is responsible for validating the values passed to the setResourceGroupExternalIds method
     * This method is willingly generated in order to preserve the one-line inline validation within the setResourceGroupExternalIds method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateResourceGroupExternalIdsForArrayConstraintsFromSetResourceGroupExternalIds(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $resourceResourceGroupExternalIdsItem) {
            // validation for constraint: itemType
            if (!is_string($resourceResourceGroupExternalIdsItem)) {
                $invalidValues[] = is_object($resourceResourceGroupExternalIdsItem) ? get_class($resourceResourceGroupExternalIdsItem) : sprintf('%s(%s)', gettype($resourceResourceGroupExternalIdsItem), var_export($resourceResourceGroupExternalIdsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The resourceGroupExternalIds property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set resourceGroupExternalIds value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param string[] $resourceGroupExternalIds
     * @return \Exerp\Access\StructType\_resource
     */
    public function setResourceGroupExternalIds(?array $resourceGroupExternalIds = null): self
    {
        // validation for constraint: array
        if ('' !== ($resourceGroupExternalIdsArrayErrorMessage = self::validateResourceGroupExternalIdsForArrayConstraintsFromSetResourceGroupExternalIds($resourceGroupExternalIds))) {
            throw new InvalidArgumentException($resourceGroupExternalIdsArrayErrorMessage, __LINE__);
        }
        if (is_null($resourceGroupExternalIds) || (is_array($resourceGroupExternalIds) && empty($resourceGroupExternalIds))) {
            unset($this->resourceGroupExternalIds);
        } else {
            $this->resourceGroupExternalIds = $resourceGroupExternalIds;
        }
        
        return $this;
    }
    /**
     * Add item to resourceGroupExternalIds value
     * @throws InvalidArgumentException
     * @param string $item
     * @return \Exerp\Access\StructType\_resource
     */
    public function addToResourceGroupExternalIds(string $item): self
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new InvalidArgumentException(sprintf('The resourceGroupExternalIds property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->resourceGroupExternalIds[] = $item;
        
        return $this;
    }
    /**
     * Get seats value
     * @return \Exerp\Access\StructType\Seats|null
     */
    public function getSeats(): ?\Exerp\Access\StructType\Seats
    {
        return $this->seats;
    }
    /**
     * Set seats value
     * @param \Exerp\Access\StructType\Seats $seats
     * @return \Exerp\Access\StructType\_resource
     */
    public function setSeats(?\Exerp\Access\StructType\Seats $seats = null): self
    {
        $this->seats = $seats;
        
        return $this;
    }
}
