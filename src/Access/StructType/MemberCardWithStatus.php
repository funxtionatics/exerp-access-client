<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for memberCardWithStatus StructType
 * @subpackage Structs
 */
class MemberCardWithStatus extends MemberCard
{
    /**
     * The status
     * @var string|null
     */
    protected ?string $status = null;
    /**
     * Constructor method for memberCardWithStatus
     * @uses MemberCardWithStatus::setStatus()
     * @param string $status
     */
    public function __construct(?string $status = null)
    {
        $this
            ->setStatus($status);
    }
    /**
     * Get status value
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }
    /**
     * Set status value
     * @uses \Exerp\Access\EnumType\MemberCardStatus::valueIsValid()
     * @uses \Exerp\Access\EnumType\MemberCardStatus::getValidValues()
     * @throws InvalidArgumentException
     * @param string $status
     * @return \Exerp\Access\StructType\MemberCardWithStatus
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Access\EnumType\MemberCardStatus::valueIsValid($status)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Access\EnumType\MemberCardStatus', is_array($status) ? implode(', ', $status) : var_export($status, true), implode(', ', \Exerp\Access\EnumType\MemberCardStatus::getValidValues())), __LINE__);
        }
        $this->status = $status;
        
        return $this;
    }
}
