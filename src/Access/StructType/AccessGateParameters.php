<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for accessGateParameters StructType
 * @subpackage Structs
 */
class AccessGateParameters extends AbstractStructBase
{
    /**
     * The memberCard
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\MemberCard|null
     */
    protected ?\Exerp\Access\StructType\MemberCard $memberCard = null;
    /**
     * The personKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Access\StructType\ApiPersonKey $personKey = null;
    /**
     * The usagePointSourceKey
     * @var \Exerp\Access\StructType\ApiUsagePointSourceKey|null
     */
    protected ?\Exerp\Access\StructType\ApiUsagePointSourceKey $usagePointSourceKey = null;
    /**
     * Constructor method for accessGateParameters
     * @uses AccessGateParameters::setMemberCard()
     * @uses AccessGateParameters::setPersonKey()
     * @uses AccessGateParameters::setUsagePointSourceKey()
     * @param \Exerp\Access\StructType\MemberCard $memberCard
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     * @param \Exerp\Access\StructType\ApiUsagePointSourceKey $usagePointSourceKey
     */
    public function __construct(?\Exerp\Access\StructType\MemberCard $memberCard = null, ?\Exerp\Access\StructType\ApiPersonKey $personKey = null, ?\Exerp\Access\StructType\ApiUsagePointSourceKey $usagePointSourceKey = null)
    {
        $this
            ->setMemberCard($memberCard)
            ->setPersonKey($personKey)
            ->setUsagePointSourceKey($usagePointSourceKey);
    }
    /**
     * Get memberCard value
     * @return \Exerp\Access\StructType\MemberCard|null
     */
    public function getMemberCard(): ?\Exerp\Access\StructType\MemberCard
    {
        return $this->memberCard;
    }
    /**
     * Set memberCard value
     * @param \Exerp\Access\StructType\MemberCard $memberCard
     * @return \Exerp\Access\StructType\AccessGateParameters
     */
    public function setMemberCard(?\Exerp\Access\StructType\MemberCard $memberCard = null): self
    {
        $this->memberCard = $memberCard;
        
        return $this;
    }
    /**
     * Get personKey value
     * @return \Exerp\Access\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Exerp\Access\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     * @return \Exerp\Access\StructType\AccessGateParameters
     */
    public function setPersonKey(?\Exerp\Access\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
    /**
     * Get usagePointSourceKey value
     * @return \Exerp\Access\StructType\ApiUsagePointSourceKey|null
     */
    public function getUsagePointSourceKey(): ?\Exerp\Access\StructType\ApiUsagePointSourceKey
    {
        return $this->usagePointSourceKey;
    }
    /**
     * Set usagePointSourceKey value
     * @param \Exerp\Access\StructType\ApiUsagePointSourceKey $usagePointSourceKey
     * @return \Exerp\Access\StructType\AccessGateParameters
     */
    public function setUsagePointSourceKey(?\Exerp\Access\StructType\ApiUsagePointSourceKey $usagePointSourceKey = null): self
    {
        $this->usagePointSourceKey = $usagePointSourceKey;
        
        return $this;
    }
}
