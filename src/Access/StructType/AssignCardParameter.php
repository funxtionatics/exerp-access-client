<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for assignCardParameter StructType
 * @subpackage Structs
 */
class AssignCardParameter extends AbstractStructBase
{
    /**
     * The personKey
     * @var \Exerp\Access\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Access\StructType\ApiPersonKey $personKey = null;
    /**
     * The card
     * @var \Exerp\Access\StructType\MemberCard|null
     */
    protected ?\Exerp\Access\StructType\MemberCard $card = null;
    /**
     * The blockExisting
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool|null
     */
    protected ?bool $blockExisting = null;
    /**
     * The blockExistingCardStatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $blockExistingCardStatus = null;
    /**
     * Constructor method for assignCardParameter
     * @uses AssignCardParameter::setPersonKey()
     * @uses AssignCardParameter::setCard()
     * @uses AssignCardParameter::setBlockExisting()
     * @uses AssignCardParameter::setBlockExistingCardStatus()
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     * @param \Exerp\Access\StructType\MemberCard $card
     * @param bool $blockExisting
     * @param string $blockExistingCardStatus
     */
    public function __construct(?\Exerp\Access\StructType\ApiPersonKey $personKey = null, ?\Exerp\Access\StructType\MemberCard $card = null, ?bool $blockExisting = null, ?string $blockExistingCardStatus = null)
    {
        $this
            ->setPersonKey($personKey)
            ->setCard($card)
            ->setBlockExisting($blockExisting)
            ->setBlockExistingCardStatus($blockExistingCardStatus);
    }
    /**
     * Get personKey value
     * @return \Exerp\Access\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Exerp\Access\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     * @return \Exerp\Access\StructType\AssignCardParameter
     */
    public function setPersonKey(?\Exerp\Access\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
    /**
     * Get card value
     * @return \Exerp\Access\StructType\MemberCard|null
     */
    public function getCard(): ?\Exerp\Access\StructType\MemberCard
    {
        return $this->card;
    }
    /**
     * Set card value
     * @param \Exerp\Access\StructType\MemberCard $card
     * @return \Exerp\Access\StructType\AssignCardParameter
     */
    public function setCard(?\Exerp\Access\StructType\MemberCard $card = null): self
    {
        $this->card = $card;
        
        return $this;
    }
    /**
     * Get blockExisting value
     * @return bool|null
     */
    public function getBlockExisting(): ?bool
    {
        return $this->blockExisting;
    }
    /**
     * Set blockExisting value
     * @param bool $blockExisting
     * @return \Exerp\Access\StructType\AssignCardParameter
     */
    public function setBlockExisting(?bool $blockExisting = null): self
    {
        // validation for constraint: boolean
        if (!is_null($blockExisting) && !is_bool($blockExisting)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($blockExisting, true), gettype($blockExisting)), __LINE__);
        }
        $this->blockExisting = $blockExisting;
        
        return $this;
    }
    /**
     * Get blockExistingCardStatus value
     * @return string|null
     */
    public function getBlockExistingCardStatus(): ?string
    {
        return $this->blockExistingCardStatus;
    }
    /**
     * Set blockExistingCardStatus value
     * @uses \Exerp\Access\EnumType\MemberCardStatus::valueIsValid()
     * @uses \Exerp\Access\EnumType\MemberCardStatus::getValidValues()
     * @throws InvalidArgumentException
     * @param string $blockExistingCardStatus
     * @return \Exerp\Access\StructType\AssignCardParameter
     */
    public function setBlockExistingCardStatus(?string $blockExistingCardStatus = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Access\EnumType\MemberCardStatus::valueIsValid($blockExistingCardStatus)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Access\EnumType\MemberCardStatus', is_array($blockExistingCardStatus) ? implode(', ', $blockExistingCardStatus) : var_export($blockExistingCardStatus, true), implode(', ', \Exerp\Access\EnumType\MemberCardStatus::getValidValues())), __LINE__);
        }
        $this->blockExistingCardStatus = $blockExistingCardStatus;
        
        return $this;
    }
}
