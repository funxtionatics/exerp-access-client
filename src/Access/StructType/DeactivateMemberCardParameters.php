<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for deactivateMemberCardParameters StructType
 * @subpackage Structs
 */
class DeactivateMemberCardParameters extends AbstractStructBase
{
    /**
     * The blockingCardStatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $blockingCardStatus = null;
    /**
     * The card
     * @var \Exerp\Access\StructType\MemberCard|null
     */
    protected ?\Exerp\Access\StructType\MemberCard $card = null;
    /**
     * The personKey
     * @var \Exerp\Access\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Access\StructType\ApiPersonKey $personKey = null;
    /**
     * Constructor method for deactivateMemberCardParameters
     * @uses DeactivateMemberCardParameters::setBlockingCardStatus()
     * @uses DeactivateMemberCardParameters::setCard()
     * @uses DeactivateMemberCardParameters::setPersonKey()
     * @param string $blockingCardStatus
     * @param \Exerp\Access\StructType\MemberCard $card
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     */
    public function __construct(?string $blockingCardStatus = null, ?\Exerp\Access\StructType\MemberCard $card = null, ?\Exerp\Access\StructType\ApiPersonKey $personKey = null)
    {
        $this
            ->setBlockingCardStatus($blockingCardStatus)
            ->setCard($card)
            ->setPersonKey($personKey);
    }
    /**
     * Get blockingCardStatus value
     * @return string|null
     */
    public function getBlockingCardStatus(): ?string
    {
        return $this->blockingCardStatus;
    }
    /**
     * Set blockingCardStatus value
     * @uses \Exerp\Access\EnumType\MemberCardStatus::valueIsValid()
     * @uses \Exerp\Access\EnumType\MemberCardStatus::getValidValues()
     * @throws InvalidArgumentException
     * @param string $blockingCardStatus
     * @return \Exerp\Access\StructType\DeactivateMemberCardParameters
     */
    public function setBlockingCardStatus(?string $blockingCardStatus = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Access\EnumType\MemberCardStatus::valueIsValid($blockingCardStatus)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Access\EnumType\MemberCardStatus', is_array($blockingCardStatus) ? implode(', ', $blockingCardStatus) : var_export($blockingCardStatus, true), implode(', ', \Exerp\Access\EnumType\MemberCardStatus::getValidValues())), __LINE__);
        }
        $this->blockingCardStatus = $blockingCardStatus;
        
        return $this;
    }
    /**
     * Get card value
     * @return \Exerp\Access\StructType\MemberCard|null
     */
    public function getCard(): ?\Exerp\Access\StructType\MemberCard
    {
        return $this->card;
    }
    /**
     * Set card value
     * @param \Exerp\Access\StructType\MemberCard $card
     * @return \Exerp\Access\StructType\DeactivateMemberCardParameters
     */
    public function setCard(?\Exerp\Access\StructType\MemberCard $card = null): self
    {
        $this->card = $card;
        
        return $this;
    }
    /**
     * Get personKey value
     * @return \Exerp\Access\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Exerp\Access\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     * @return \Exerp\Access\StructType\DeactivateMemberCardParameters
     */
    public function setPersonKey(?\Exerp\Access\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
}
