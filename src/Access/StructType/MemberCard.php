<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for memberCard StructType
 * @subpackage Structs
 */
class MemberCard extends AbstractStructBase
{
    /**
     * The cardId
     * @var string|null
     */
    protected ?string $cardId = null;
    /**
     * The cardType
     * @var string|null
     */
    protected ?string $cardType = null;
    /**
     * Constructor method for memberCard
     * @uses MemberCard::setCardId()
     * @uses MemberCard::setCardType()
     * @param string $cardId
     * @param string $cardType
     */
    public function __construct(?string $cardId = null, ?string $cardType = null)
    {
        $this
            ->setCardId($cardId)
            ->setCardType($cardType);
    }
    /**
     * Get cardId value
     * @return string|null
     */
    public function getCardId(): ?string
    {
        return $this->cardId;
    }
    /**
     * Set cardId value
     * @param string $cardId
     * @return \Exerp\Access\StructType\MemberCard
     */
    public function setCardId(?string $cardId = null): self
    {
        // validation for constraint: string
        if (!is_null($cardId) && !is_string($cardId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cardId, true), gettype($cardId)), __LINE__);
        }
        $this->cardId = $cardId;
        
        return $this;
    }
    /**
     * Get cardType value
     * @return string|null
     */
    public function getCardType(): ?string
    {
        return $this->cardType;
    }
    /**
     * Set cardType value
     * @uses \Exerp\Access\EnumType\AccessCardType::valueIsValid()
     * @uses \Exerp\Access\EnumType\AccessCardType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $cardType
     * @return \Exerp\Access\StructType\MemberCard
     */
    public function setCardType(?string $cardType = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Access\EnumType\AccessCardType::valueIsValid($cardType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Access\EnumType\AccessCardType', is_array($cardType) ? implode(', ', $cardType) : var_export($cardType, true), implode(', ', \Exerp\Access\EnumType\AccessCardType::getValidValues())), __LINE__);
        }
        $this->cardType = $cardType;
        
        return $this;
    }
}
