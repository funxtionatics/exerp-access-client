<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for personName StructType
 * @subpackage Structs
 */
class PersonName extends AbstractStructBase
{
    /**
     * The first
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $first = null;
    /**
     * The full
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $full = null;
    /**
     * The last
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $last = null;
    /**
     * The middle
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $middle = null;
    /**
     * The nick
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $nick = null;
    /**
     * Constructor method for personName
     * @uses PersonName::setFirst()
     * @uses PersonName::setFull()
     * @uses PersonName::setLast()
     * @uses PersonName::setMiddle()
     * @uses PersonName::setNick()
     * @param string $first
     * @param string $full
     * @param string $last
     * @param string $middle
     * @param string $nick
     */
    public function __construct(?string $first = null, ?string $full = null, ?string $last = null, ?string $middle = null, ?string $nick = null)
    {
        $this
            ->setFirst($first)
            ->setFull($full)
            ->setLast($last)
            ->setMiddle($middle)
            ->setNick($nick);
    }
    /**
     * Get first value
     * @return string|null
     */
    public function getFirst(): ?string
    {
        return $this->first;
    }
    /**
     * Set first value
     * @param string $first
     * @return \Exerp\Access\StructType\PersonName
     */
    public function setFirst(?string $first = null): self
    {
        // validation for constraint: string
        if (!is_null($first) && !is_string($first)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($first, true), gettype($first)), __LINE__);
        }
        $this->first = $first;
        
        return $this;
    }
    /**
     * Get full value
     * @return string|null
     */
    public function getFull(): ?string
    {
        return $this->full;
    }
    /**
     * Set full value
     * @param string $full
     * @return \Exerp\Access\StructType\PersonName
     */
    public function setFull(?string $full = null): self
    {
        // validation for constraint: string
        if (!is_null($full) && !is_string($full)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($full, true), gettype($full)), __LINE__);
        }
        $this->full = $full;
        
        return $this;
    }
    /**
     * Get last value
     * @return string|null
     */
    public function getLast(): ?string
    {
        return $this->last;
    }
    /**
     * Set last value
     * @param string $last
     * @return \Exerp\Access\StructType\PersonName
     */
    public function setLast(?string $last = null): self
    {
        // validation for constraint: string
        if (!is_null($last) && !is_string($last)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($last, true), gettype($last)), __LINE__);
        }
        $this->last = $last;
        
        return $this;
    }
    /**
     * Get middle value
     * @return string|null
     */
    public function getMiddle(): ?string
    {
        return $this->middle;
    }
    /**
     * Set middle value
     * @param string $middle
     * @return \Exerp\Access\StructType\PersonName
     */
    public function setMiddle(?string $middle = null): self
    {
        // validation for constraint: string
        if (!is_null($middle) && !is_string($middle)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($middle, true), gettype($middle)), __LINE__);
        }
        $this->middle = $middle;
        
        return $this;
    }
    /**
     * Get nick value
     * @return string|null
     */
    public function getNick(): ?string
    {
        return $this->nick;
    }
    /**
     * Set nick value
     * @param string $nick
     * @return \Exerp\Access\StructType\PersonName
     */
    public function setNick(?string $nick = null): self
    {
        // validation for constraint: string
        if (!is_null($nick) && !is_string($nick)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nick, true), gettype($nick)), __LINE__);
        }
        $this->nick = $nick;
        
        return $this;
    }
}
