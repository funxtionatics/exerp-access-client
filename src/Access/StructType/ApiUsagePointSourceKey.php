<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for apiUsagePointSourceKey StructType
 * @subpackage Structs
 */
class ApiUsagePointSourceKey extends AbstractStructBase
{
    /**
     * The center
     * @var int|null
     */
    protected ?int $center = null;
    /**
     * The externalId
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * Constructor method for apiUsagePointSourceKey
     * @uses ApiUsagePointSourceKey::setCenter()
     * @uses ApiUsagePointSourceKey::setExternalId()
     * @param int $center
     * @param string $externalId
     */
    public function __construct(?int $center = null, ?string $externalId = null)
    {
        $this
            ->setCenter($center)
            ->setExternalId($externalId);
    }
    /**
     * Get center value
     * @return int|null
     */
    public function getCenter(): ?int
    {
        return $this->center;
    }
    /**
     * Set center value
     * @param int $center
     * @return \Exerp\Access\StructType\ApiUsagePointSourceKey
     */
    public function setCenter(?int $center = null): self
    {
        // validation for constraint: int
        if (!is_null($center) && !(is_int($center) || ctype_digit($center))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($center, true), gettype($center)), __LINE__);
        }
        $this->center = $center;
        
        return $this;
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Exerp\Access\StructType\ApiUsagePointSourceKey
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
}
