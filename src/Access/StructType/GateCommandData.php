<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for gateCommandData StructType
 * @subpackage Structs
 */
class GateCommandData extends AbstractStructBase
{
    /**
     * The deviceId
     * @var int|null
     */
    protected ?int $deviceId = null;
    /**
     * The deviceSubId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $deviceSubId = null;
    /**
     * The gateKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\CompositeKey|null
     */
    protected ?\Exerp\Access\StructType\CompositeKey $gateKey = null;
    /**
     * The grantAccess
     * @var bool|null
     */
    protected ?bool $grantAccess = null;
    /**
     * Constructor method for gateCommandData
     * @uses GateCommandData::setDeviceId()
     * @uses GateCommandData::setDeviceSubId()
     * @uses GateCommandData::setGateKey()
     * @uses GateCommandData::setGrantAccess()
     * @param int $deviceId
     * @param string $deviceSubId
     * @param \Exerp\Access\StructType\CompositeKey $gateKey
     * @param bool $grantAccess
     */
    public function __construct(?int $deviceId = null, ?string $deviceSubId = null, ?\Exerp\Access\StructType\CompositeKey $gateKey = null, ?bool $grantAccess = null)
    {
        $this
            ->setDeviceId($deviceId)
            ->setDeviceSubId($deviceSubId)
            ->setGateKey($gateKey)
            ->setGrantAccess($grantAccess);
    }
    /**
     * Get deviceId value
     * @return int|null
     */
    public function getDeviceId(): ?int
    {
        return $this->deviceId;
    }
    /**
     * Set deviceId value
     * @param int $deviceId
     * @return \Exerp\Access\StructType\GateCommandData
     */
    public function setDeviceId(?int $deviceId = null): self
    {
        // validation for constraint: int
        if (!is_null($deviceId) && !(is_int($deviceId) || ctype_digit($deviceId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($deviceId, true), gettype($deviceId)), __LINE__);
        }
        $this->deviceId = $deviceId;
        
        return $this;
    }
    /**
     * Get deviceSubId value
     * @return string|null
     */
    public function getDeviceSubId(): ?string
    {
        return $this->deviceSubId;
    }
    /**
     * Set deviceSubId value
     * @param string $deviceSubId
     * @return \Exerp\Access\StructType\GateCommandData
     */
    public function setDeviceSubId(?string $deviceSubId = null): self
    {
        // validation for constraint: string
        if (!is_null($deviceSubId) && !is_string($deviceSubId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deviceSubId, true), gettype($deviceSubId)), __LINE__);
        }
        $this->deviceSubId = $deviceSubId;
        
        return $this;
    }
    /**
     * Get gateKey value
     * @return \Exerp\Access\StructType\CompositeKey|null
     */
    public function getGateKey(): ?\Exerp\Access\StructType\CompositeKey
    {
        return $this->gateKey;
    }
    /**
     * Set gateKey value
     * @param \Exerp\Access\StructType\CompositeKey $gateKey
     * @return \Exerp\Access\StructType\GateCommandData
     */
    public function setGateKey(?\Exerp\Access\StructType\CompositeKey $gateKey = null): self
    {
        $this->gateKey = $gateKey;
        
        return $this;
    }
    /**
     * Get grantAccess value
     * @return bool|null
     */
    public function getGrantAccess(): ?bool
    {
        return $this->grantAccess;
    }
    /**
     * Set grantAccess value
     * @param bool $grantAccess
     * @return \Exerp\Access\StructType\GateCommandData
     */
    public function setGrantAccess(?bool $grantAccess = null): self
    {
        // validation for constraint: boolean
        if (!is_null($grantAccess) && !is_bool($grantAccess)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($grantAccess, true), gettype($grantAccess)), __LINE__);
        }
        $this->grantAccess = $grantAccess;
        
        return $this;
    }
}
