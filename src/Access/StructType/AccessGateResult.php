<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for accessGateResult StructType
 * @subpackage Structs
 */
class AccessGateResult extends AbstractStructBase
{
    /**
     * The actionKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\CompositeKey|null
     */
    protected ?\Exerp\Access\StructType\CompositeKey $actionKey = null;
    /**
     * The actionName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $actionName = null;
    /**
     * The gateCommandData
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\GateCommandData|null
     */
    protected ?\Exerp\Access\StructType\GateCommandData $gateCommandData = null;
    /**
     * The person
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\PersonSimple|null
     */
    protected ?\Exerp\Access\StructType\PersonSimple $person = null;
    /**
     * The usagePointKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\CompositeKey|null
     */
    protected ?\Exerp\Access\StructType\CompositeKey $usagePointKey = null;
    /**
     * The usagePointName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $usagePointName = null;
    /**
     * Constructor method for accessGateResult
     * @uses AccessGateResult::setActionKey()
     * @uses AccessGateResult::setActionName()
     * @uses AccessGateResult::setGateCommandData()
     * @uses AccessGateResult::setPerson()
     * @uses AccessGateResult::setUsagePointKey()
     * @uses AccessGateResult::setUsagePointName()
     * @param \Exerp\Access\StructType\CompositeKey $actionKey
     * @param string $actionName
     * @param \Exerp\Access\StructType\GateCommandData $gateCommandData
     * @param \Exerp\Access\StructType\PersonSimple $person
     * @param \Exerp\Access\StructType\CompositeKey $usagePointKey
     * @param string $usagePointName
     */
    public function __construct(?\Exerp\Access\StructType\CompositeKey $actionKey = null, ?string $actionName = null, ?\Exerp\Access\StructType\GateCommandData $gateCommandData = null, ?\Exerp\Access\StructType\PersonSimple $person = null, ?\Exerp\Access\StructType\CompositeKey $usagePointKey = null, ?string $usagePointName = null)
    {
        $this
            ->setActionKey($actionKey)
            ->setActionName($actionName)
            ->setGateCommandData($gateCommandData)
            ->setPerson($person)
            ->setUsagePointKey($usagePointKey)
            ->setUsagePointName($usagePointName);
    }
    /**
     * Get actionKey value
     * @return \Exerp\Access\StructType\CompositeKey|null
     */
    public function getActionKey(): ?\Exerp\Access\StructType\CompositeKey
    {
        return $this->actionKey;
    }
    /**
     * Set actionKey value
     * @param \Exerp\Access\StructType\CompositeKey $actionKey
     * @return \Exerp\Access\StructType\AccessGateResult
     */
    public function setActionKey(?\Exerp\Access\StructType\CompositeKey $actionKey = null): self
    {
        $this->actionKey = $actionKey;
        
        return $this;
    }
    /**
     * Get actionName value
     * @return string|null
     */
    public function getActionName(): ?string
    {
        return $this->actionName;
    }
    /**
     * Set actionName value
     * @param string $actionName
     * @return \Exerp\Access\StructType\AccessGateResult
     */
    public function setActionName(?string $actionName = null): self
    {
        // validation for constraint: string
        if (!is_null($actionName) && !is_string($actionName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($actionName, true), gettype($actionName)), __LINE__);
        }
        $this->actionName = $actionName;
        
        return $this;
    }
    /**
     * Get gateCommandData value
     * @return \Exerp\Access\StructType\GateCommandData|null
     */
    public function getGateCommandData(): ?\Exerp\Access\StructType\GateCommandData
    {
        return $this->gateCommandData;
    }
    /**
     * Set gateCommandData value
     * @param \Exerp\Access\StructType\GateCommandData $gateCommandData
     * @return \Exerp\Access\StructType\AccessGateResult
     */
    public function setGateCommandData(?\Exerp\Access\StructType\GateCommandData $gateCommandData = null): self
    {
        $this->gateCommandData = $gateCommandData;
        
        return $this;
    }
    /**
     * Get person value
     * @return \Exerp\Access\StructType\PersonSimple|null
     */
    public function getPerson(): ?\Exerp\Access\StructType\PersonSimple
    {
        return $this->person;
    }
    /**
     * Set person value
     * @param \Exerp\Access\StructType\PersonSimple $person
     * @return \Exerp\Access\StructType\AccessGateResult
     */
    public function setPerson(?\Exerp\Access\StructType\PersonSimple $person = null): self
    {
        $this->person = $person;
        
        return $this;
    }
    /**
     * Get usagePointKey value
     * @return \Exerp\Access\StructType\CompositeKey|null
     */
    public function getUsagePointKey(): ?\Exerp\Access\StructType\CompositeKey
    {
        return $this->usagePointKey;
    }
    /**
     * Set usagePointKey value
     * @param \Exerp\Access\StructType\CompositeKey $usagePointKey
     * @return \Exerp\Access\StructType\AccessGateResult
     */
    public function setUsagePointKey(?\Exerp\Access\StructType\CompositeKey $usagePointKey = null): self
    {
        $this->usagePointKey = $usagePointKey;
        
        return $this;
    }
    /**
     * Get usagePointName value
     * @return string|null
     */
    public function getUsagePointName(): ?string
    {
        return $this->usagePointName;
    }
    /**
     * Set usagePointName value
     * @param string $usagePointName
     * @return \Exerp\Access\StructType\AccessGateResult
     */
    public function setUsagePointName(?string $usagePointName = null): self
    {
        // validation for constraint: string
        if (!is_null($usagePointName) && !is_string($usagePointName)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($usagePointName, true), gettype($usagePointName)), __LINE__);
        }
        $this->usagePointName = $usagePointName;
        
        return $this;
    }
}
