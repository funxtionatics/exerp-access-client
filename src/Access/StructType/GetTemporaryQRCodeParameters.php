<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getTemporaryQRCodeParameters StructType
 * @subpackage Structs
 */
class GetTemporaryQRCodeParameters extends AbstractStructBase
{
    /**
     * The personKey
     * @var \Exerp\Access\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Access\StructType\ApiPersonKey $personKey = null;
    /**
     * Constructor method for getTemporaryQRCodeParameters
     * @uses GetTemporaryQRCodeParameters::setPersonKey()
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     */
    public function __construct(?\Exerp\Access\StructType\ApiPersonKey $personKey = null)
    {
        $this
            ->setPersonKey($personKey);
    }
    /**
     * Get personKey value
     * @return \Exerp\Access\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Exerp\Access\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     * @return \Exerp\Access\StructType\GetTemporaryQRCodeParameters
     */
    public function setPersonKey(?\Exerp\Access\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
}
