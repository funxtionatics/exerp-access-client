<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getUsagePointSourceResponse StructType
 * @subpackage Structs
 */
class GetUsagePointSourceResponse extends AbstractStructBase
{
    /**
     * The usagePointSources
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\UsagePointSources|null
     */
    protected ?\Exerp\Access\StructType\UsagePointSources $usagePointSources = null;
    /**
     * Constructor method for getUsagePointSourceResponse
     * @uses GetUsagePointSourceResponse::setUsagePointSources()
     * @param \Exerp\Access\StructType\UsagePointSources $usagePointSources
     */
    public function __construct(?\Exerp\Access\StructType\UsagePointSources $usagePointSources = null)
    {
        $this
            ->setUsagePointSources($usagePointSources);
    }
    /**
     * Get usagePointSources value
     * @return \Exerp\Access\StructType\UsagePointSources|null
     */
    public function getUsagePointSources(): ?\Exerp\Access\StructType\UsagePointSources
    {
        return $this->usagePointSources;
    }
    /**
     * Set usagePointSources value
     * @param \Exerp\Access\StructType\UsagePointSources $usagePointSources
     * @return \Exerp\Access\StructType\GetUsagePointSourceResponse
     */
    public function setUsagePointSources(?\Exerp\Access\StructType\UsagePointSources $usagePointSources = null): self
    {
        $this->usagePointSources = $usagePointSources;
        
        return $this;
    }
}
