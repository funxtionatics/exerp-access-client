<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for usagePointSource StructType
 * @subpackage Structs
 */
class UsagePointSource extends AbstractStructBase
{
    /**
     * The key
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\FederatedIntegerKey|null
     */
    protected ?\Exerp\Access\StructType\FederatedIntegerKey $key = null;
    /**
     * The client
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $client = null;
    /**
     * The device
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $device = null;
    /**
     * The action
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $action = null;
    /**
     * The externalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $externalId = null;
    /**
     * Constructor method for usagePointSource
     * @uses UsagePointSource::setKey()
     * @uses UsagePointSource::setClient()
     * @uses UsagePointSource::setDevice()
     * @uses UsagePointSource::setAction()
     * @uses UsagePointSource::setExternalId()
     * @param \Exerp\Access\StructType\FederatedIntegerKey $key
     * @param string $client
     * @param string $device
     * @param string $action
     * @param string $externalId
     */
    public function __construct(?\Exerp\Access\StructType\FederatedIntegerKey $key = null, ?string $client = null, ?string $device = null, ?string $action = null, ?string $externalId = null)
    {
        $this
            ->setKey($key)
            ->setClient($client)
            ->setDevice($device)
            ->setAction($action)
            ->setExternalId($externalId);
    }
    /**
     * Get key value
     * @return \Exerp\Access\StructType\FederatedIntegerKey|null
     */
    public function getKey(): ?\Exerp\Access\StructType\FederatedIntegerKey
    {
        return $this->key;
    }
    /**
     * Set key value
     * @param \Exerp\Access\StructType\FederatedIntegerKey $key
     * @return \Exerp\Access\StructType\UsagePointSource
     */
    public function setKey(?\Exerp\Access\StructType\FederatedIntegerKey $key = null): self
    {
        $this->key = $key;
        
        return $this;
    }
    /**
     * Get client value
     * @return string|null
     */
    public function getClient(): ?string
    {
        return $this->client;
    }
    /**
     * Set client value
     * @param string $client
     * @return \Exerp\Access\StructType\UsagePointSource
     */
    public function setClient(?string $client = null): self
    {
        // validation for constraint: string
        if (!is_null($client) && !is_string($client)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($client, true), gettype($client)), __LINE__);
        }
        $this->client = $client;
        
        return $this;
    }
    /**
     * Get device value
     * @return string|null
     */
    public function getDevice(): ?string
    {
        return $this->device;
    }
    /**
     * Set device value
     * @param string $device
     * @return \Exerp\Access\StructType\UsagePointSource
     */
    public function setDevice(?string $device = null): self
    {
        // validation for constraint: string
        if (!is_null($device) && !is_string($device)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($device, true), gettype($device)), __LINE__);
        }
        $this->device = $device;
        
        return $this;
    }
    /**
     * Get action value
     * @return string|null
     */
    public function getAction(): ?string
    {
        return $this->action;
    }
    /**
     * Set action value
     * @param string $action
     * @return \Exerp\Access\StructType\UsagePointSource
     */
    public function setAction(?string $action = null): self
    {
        // validation for constraint: string
        if (!is_null($action) && !is_string($action)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($action, true), gettype($action)), __LINE__);
        }
        $this->action = $action;
        
        return $this;
    }
    /**
     * Get externalId value
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }
    /**
     * Set externalId value
     * @param string $externalId
     * @return \Exerp\Access\StructType\UsagePointSource
     */
    public function setExternalId(?string $externalId = null): self
    {
        // validation for constraint: string
        if (!is_null($externalId) && !is_string($externalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalId, true), gettype($externalId)), __LINE__);
        }
        $this->externalId = $externalId;
        
        return $this;
    }
}
