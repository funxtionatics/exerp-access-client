<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for personSimple StructType
 * @subpackage Structs
 */
class PersonSimple extends AbstractStructBase
{
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\PersonName|null
     */
    protected ?\Exerp\Access\StructType\PersonName $name = null;
    /**
     * The personKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Access\StructType\ApiPersonKey $personKey = null;
    /**
     * Constructor method for personSimple
     * @uses PersonSimple::setName()
     * @uses PersonSimple::setPersonKey()
     * @param \Exerp\Access\StructType\PersonName $name
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     */
    public function __construct(?\Exerp\Access\StructType\PersonName $name = null, ?\Exerp\Access\StructType\ApiPersonKey $personKey = null)
    {
        $this
            ->setName($name)
            ->setPersonKey($personKey);
    }
    /**
     * Get name value
     * @return \Exerp\Access\StructType\PersonName|null
     */
    public function getName(): ?\Exerp\Access\StructType\PersonName
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param \Exerp\Access\StructType\PersonName $name
     * @return \Exerp\Access\StructType\PersonSimple
     */
    public function setName(?\Exerp\Access\StructType\PersonName $name = null): self
    {
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get personKey value
     * @return \Exerp\Access\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Exerp\Access\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     * @return \Exerp\Access\StructType\PersonSimple
     */
    public function setPersonKey(?\Exerp\Access\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
}
