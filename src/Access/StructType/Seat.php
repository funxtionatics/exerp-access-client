<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for seat StructType
 * @subpackage Structs
 */
class Seat extends AbstractStructBase
{
    /**
     * The name
     * @var string|null
     */
    protected ?string $name = null;
    /**
     * The position
     * @var float|null
     */
    protected ?float $position = null;
    /**
     * The row
     * @var float|null
     */
    protected ?float $row = null;
    /**
     * Constructor method for seat
     * @uses Seat::setName()
     * @uses Seat::setPosition()
     * @uses Seat::setRow()
     * @param string $name
     * @param float $position
     * @param float $row
     */
    public function __construct(?string $name = null, ?float $position = null, ?float $row = null)
    {
        $this
            ->setName($name)
            ->setPosition($position)
            ->setRow($row);
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \Exerp\Access\StructType\Seat
     */
    public function setName(?string $name = null): self
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        
        return $this;
    }
    /**
     * Get position value
     * @return float|null
     */
    public function getPosition(): ?float
    {
        return $this->position;
    }
    /**
     * Set position value
     * @param float $position
     * @return \Exerp\Access\StructType\Seat
     */
    public function setPosition(?float $position = null): self
    {
        // validation for constraint: float
        if (!is_null($position) && !(is_float($position) || is_numeric($position))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($position, true), gettype($position)), __LINE__);
        }
        $this->position = $position;
        
        return $this;
    }
    /**
     * Get row value
     * @return float|null
     */
    public function getRow(): ?float
    {
        return $this->row;
    }
    /**
     * Set row value
     * @param float $row
     * @return \Exerp\Access\StructType\Seat
     */
    public function setRow(?float $row = null): self
    {
        // validation for constraint: float
        if (!is_null($row) && !(is_float($row) || is_numeric($row))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($row, true), gettype($row)), __LINE__);
        }
        $this->row = $row;
        
        return $this;
    }
}
