<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for usagePointSources StructType
 * @subpackage Structs
 */
class UsagePointSources extends AbstractStructBase
{
    /**
     * The usagePointSource
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\UsagePointSource[]
     */
    protected ?array $usagePointSource = null;
    /**
     * Constructor method for usagePointSources
     * @uses UsagePointSources::setUsagePointSource()
     * @param \Exerp\Access\StructType\UsagePointSource[] $usagePointSource
     */
    public function __construct(?array $usagePointSource = null)
    {
        $this
            ->setUsagePointSource($usagePointSource);
    }
    /**
     * Get usagePointSource value
     * @return \Exerp\Access\StructType\UsagePointSource[]
     */
    public function getUsagePointSource(): ?array
    {
        return $this->usagePointSource;
    }
    /**
     * This method is responsible for validating the values passed to the setUsagePointSource method
     * This method is willingly generated in order to preserve the one-line inline validation within the setUsagePointSource method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateUsagePointSourceForArrayConstraintsFromSetUsagePointSource(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $usagePointSourcesUsagePointSourceItem) {
            // validation for constraint: itemType
            if (!$usagePointSourcesUsagePointSourceItem instanceof \Exerp\Access\StructType\UsagePointSource) {
                $invalidValues[] = is_object($usagePointSourcesUsagePointSourceItem) ? get_class($usagePointSourcesUsagePointSourceItem) : sprintf('%s(%s)', gettype($usagePointSourcesUsagePointSourceItem), var_export($usagePointSourcesUsagePointSourceItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The usagePointSource property can only contain items of type \Exerp\Access\StructType\UsagePointSource, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set usagePointSource value
     * @throws InvalidArgumentException
     * @param \Exerp\Access\StructType\UsagePointSource[] $usagePointSource
     * @return \Exerp\Access\StructType\UsagePointSources
     */
    public function setUsagePointSource(?array $usagePointSource = null): self
    {
        // validation for constraint: array
        if ('' !== ($usagePointSourceArrayErrorMessage = self::validateUsagePointSourceForArrayConstraintsFromSetUsagePointSource($usagePointSource))) {
            throw new InvalidArgumentException($usagePointSourceArrayErrorMessage, __LINE__);
        }
        $this->usagePointSource = $usagePointSource;
        
        return $this;
    }
    /**
     * Add item to usagePointSource value
     * @throws InvalidArgumentException
     * @param \Exerp\Access\StructType\UsagePointSource $item
     * @return \Exerp\Access\StructType\UsagePointSources
     */
    public function addToUsagePointSource(\Exerp\Access\StructType\UsagePointSource $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Access\StructType\UsagePointSource) {
            throw new InvalidArgumentException(sprintf('The usagePointSource property can only contain items of type \Exerp\Access\StructType\UsagePointSource, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->usagePointSource[] = $item;
        
        return $this;
    }
}
