<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for resourcesAvailableForAttendsParameters StructType
 * @subpackage Structs
 */
class ResourcesAvailableForAttendsParameters extends AbstractStructBase
{
    /**
     * The centerId
     * @var int|null
     */
    protected ?int $centerId = null;
    /**
     * The personKey
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Access\StructType\ApiPersonKey $personKey = null;
    /**
     * The resourceExternalId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $resourceExternalId = null;
    /**
     * Constructor method for resourcesAvailableForAttendsParameters
     * @uses ResourcesAvailableForAttendsParameters::setCenterId()
     * @uses ResourcesAvailableForAttendsParameters::setPersonKey()
     * @uses ResourcesAvailableForAttendsParameters::setResourceExternalId()
     * @param int $centerId
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     * @param string $resourceExternalId
     */
    public function __construct(?int $centerId = null, ?\Exerp\Access\StructType\ApiPersonKey $personKey = null, ?string $resourceExternalId = null)
    {
        $this
            ->setCenterId($centerId)
            ->setPersonKey($personKey)
            ->setResourceExternalId($resourceExternalId);
    }
    /**
     * Get centerId value
     * @return int|null
     */
    public function getCenterId(): ?int
    {
        return $this->centerId;
    }
    /**
     * Set centerId value
     * @param int $centerId
     * @return \Exerp\Access\StructType\ResourcesAvailableForAttendsParameters
     */
    public function setCenterId(?int $centerId = null): self
    {
        // validation for constraint: int
        if (!is_null($centerId) && !(is_int($centerId) || ctype_digit($centerId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($centerId, true), gettype($centerId)), __LINE__);
        }
        $this->centerId = $centerId;
        
        return $this;
    }
    /**
     * Get personKey value
     * @return \Exerp\Access\StructType\ApiPersonKey|null
     */
    public function getPersonKey(): ?\Exerp\Access\StructType\ApiPersonKey
    {
        return $this->personKey;
    }
    /**
     * Set personKey value
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     * @return \Exerp\Access\StructType\ResourcesAvailableForAttendsParameters
     */
    public function setPersonKey(?\Exerp\Access\StructType\ApiPersonKey $personKey = null): self
    {
        $this->personKey = $personKey;
        
        return $this;
    }
    /**
     * Get resourceExternalId value
     * @return string|null
     */
    public function getResourceExternalId(): ?string
    {
        return $this->resourceExternalId;
    }
    /**
     * Set resourceExternalId value
     * @param string $resourceExternalId
     * @return \Exerp\Access\StructType\ResourcesAvailableForAttendsParameters
     */
    public function setResourceExternalId(?string $resourceExternalId = null): self
    {
        // validation for constraint: string
        if (!is_null($resourceExternalId) && !is_string($resourceExternalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($resourceExternalId, true), gettype($resourceExternalId)), __LINE__);
        }
        $this->resourceExternalId = $resourceExternalId;
        
        return $this;
    }
}
