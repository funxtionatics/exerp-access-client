<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for centerPresence StructType
 * @subpackage Structs
 */
class CenterPresence extends AbstractStructBase
{
    /**
     * The center
     * @var int|null
     */
    protected ?int $center = null;
    /**
     * The date
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $date = null;
    /**
     * The personId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\ApiPersonKey|null
     */
    protected ?\Exerp\Access\StructType\ApiPersonKey $personId = null;
    /**
     * The roomResource
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $roomResource = null;
    /**
     * The startTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $startTime = null;
    /**
     * The status
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $status = null;
    /**
     * The stopTime
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $stopTime = null;
    /**
     * The type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $type = null;
    /**
     * Constructor method for centerPresence
     * @uses CenterPresence::setCenter()
     * @uses CenterPresence::setDate()
     * @uses CenterPresence::setPersonId()
     * @uses CenterPresence::setRoomResource()
     * @uses CenterPresence::setStartTime()
     * @uses CenterPresence::setStatus()
     * @uses CenterPresence::setStopTime()
     * @uses CenterPresence::setType()
     * @param int $center
     * @param string $date
     * @param \Exerp\Access\StructType\ApiPersonKey $personId
     * @param string $roomResource
     * @param string $startTime
     * @param string $status
     * @param string $stopTime
     * @param string $type
     */
    public function __construct(?int $center = null, ?string $date = null, ?\Exerp\Access\StructType\ApiPersonKey $personId = null, ?string $roomResource = null, ?string $startTime = null, ?string $status = null, ?string $stopTime = null, ?string $type = null)
    {
        $this
            ->setCenter($center)
            ->setDate($date)
            ->setPersonId($personId)
            ->setRoomResource($roomResource)
            ->setStartTime($startTime)
            ->setStatus($status)
            ->setStopTime($stopTime)
            ->setType($type);
    }
    /**
     * Get center value
     * @return int|null
     */
    public function getCenter(): ?int
    {
        return $this->center;
    }
    /**
     * Set center value
     * @param int $center
     * @return \Exerp\Access\StructType\CenterPresence
     */
    public function setCenter(?int $center = null): self
    {
        // validation for constraint: int
        if (!is_null($center) && !(is_int($center) || ctype_digit($center))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($center, true), gettype($center)), __LINE__);
        }
        $this->center = $center;
        
        return $this;
    }
    /**
     * Get date value
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }
    /**
     * Set date value
     * @param string $date
     * @return \Exerp\Access\StructType\CenterPresence
     */
    public function setDate(?string $date = null): self
    {
        // validation for constraint: string
        if (!is_null($date) && !is_string($date)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($date, true), gettype($date)), __LINE__);
        }
        $this->date = $date;
        
        return $this;
    }
    /**
     * Get personId value
     * @return \Exerp\Access\StructType\ApiPersonKey|null
     */
    public function getPersonId(): ?\Exerp\Access\StructType\ApiPersonKey
    {
        return $this->personId;
    }
    /**
     * Set personId value
     * @param \Exerp\Access\StructType\ApiPersonKey $personId
     * @return \Exerp\Access\StructType\CenterPresence
     */
    public function setPersonId(?\Exerp\Access\StructType\ApiPersonKey $personId = null): self
    {
        $this->personId = $personId;
        
        return $this;
    }
    /**
     * Get roomResource value
     * @return string|null
     */
    public function getRoomResource(): ?string
    {
        return $this->roomResource;
    }
    /**
     * Set roomResource value
     * @param string $roomResource
     * @return \Exerp\Access\StructType\CenterPresence
     */
    public function setRoomResource(?string $roomResource = null): self
    {
        // validation for constraint: string
        if (!is_null($roomResource) && !is_string($roomResource)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($roomResource, true), gettype($roomResource)), __LINE__);
        }
        $this->roomResource = $roomResource;
        
        return $this;
    }
    /**
     * Get startTime value
     * @return string|null
     */
    public function getStartTime(): ?string
    {
        return $this->startTime;
    }
    /**
     * Set startTime value
     * @param string $startTime
     * @return \Exerp\Access\StructType\CenterPresence
     */
    public function setStartTime(?string $startTime = null): self
    {
        // validation for constraint: string
        if (!is_null($startTime) && !is_string($startTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startTime, true), gettype($startTime)), __LINE__);
        }
        $this->startTime = $startTime;
        
        return $this;
    }
    /**
     * Get status value
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }
    /**
     * Set status value
     * @param string $status
     * @return \Exerp\Access\StructType\CenterPresence
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: string
        if (!is_null($status) && !is_string($status)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($status, true), gettype($status)), __LINE__);
        }
        $this->status = $status;
        
        return $this;
    }
    /**
     * Get stopTime value
     * @return string|null
     */
    public function getStopTime(): ?string
    {
        return $this->stopTime;
    }
    /**
     * Set stopTime value
     * @param string $stopTime
     * @return \Exerp\Access\StructType\CenterPresence
     */
    public function setStopTime(?string $stopTime = null): self
    {
        // validation for constraint: string
        if (!is_null($stopTime) && !is_string($stopTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($stopTime, true), gettype($stopTime)), __LINE__);
        }
        $this->stopTime = $stopTime;
        
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }
    /**
     * Set type value
     * @uses \Exerp\Access\EnumType\CenterPresenceType::valueIsValid()
     * @uses \Exerp\Access\EnumType\CenterPresenceType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $type
     * @return \Exerp\Access\StructType\CenterPresence
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Access\EnumType\CenterPresenceType::valueIsValid($type)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Access\EnumType\CenterPresenceType', is_array($type) ? implode(', ', $type) : var_export($type, true), implode(', ', \Exerp\Access\EnumType\CenterPresenceType::getValidValues())), __LINE__);
        }
        $this->type = $type;
        
        return $this;
    }
}
