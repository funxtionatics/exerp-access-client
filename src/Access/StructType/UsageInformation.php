<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for usageInformation StructType
 * @subpackage Structs
 */
class UsageInformation extends AbstractStructBase
{
    /**
     * The balance
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $balance = null;
    /**
     * The clips
     * @var int|null
     */
    protected ?int $clips = null;
    /**
     * The clipsLeft
     * @var int|null
     */
    protected ?int $clipsLeft = null;
    /**
     * The expireDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $expireDate = null;
    /**
     * The price
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string|null
     */
    protected ?string $price = null;
    /**
     * Constructor method for usageInformation
     * @uses UsageInformation::setBalance()
     * @uses UsageInformation::setClips()
     * @uses UsageInformation::setClipsLeft()
     * @uses UsageInformation::setExpireDate()
     * @uses UsageInformation::setPrice()
     * @param string $balance
     * @param int $clips
     * @param int $clipsLeft
     * @param string $expireDate
     * @param string $price
     */
    public function __construct(?string $balance = null, ?int $clips = null, ?int $clipsLeft = null, ?string $expireDate = null, ?string $price = null)
    {
        $this
            ->setBalance($balance)
            ->setClips($clips)
            ->setClipsLeft($clipsLeft)
            ->setExpireDate($expireDate)
            ->setPrice($price);
    }
    /**
     * Get balance value
     * @return string|null
     */
    public function getBalance(): ?string
    {
        return $this->balance;
    }
    /**
     * Set balance value
     * @param string $balance
     * @return \Exerp\Access\StructType\UsageInformation
     */
    public function setBalance(?string $balance = null): self
    {
        // validation for constraint: string
        if (!is_null($balance) && !is_string($balance)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($balance, true), gettype($balance)), __LINE__);
        }
        $this->balance = $balance;
        
        return $this;
    }
    /**
     * Get clips value
     * @return int|null
     */
    public function getClips(): ?int
    {
        return $this->clips;
    }
    /**
     * Set clips value
     * @param int $clips
     * @return \Exerp\Access\StructType\UsageInformation
     */
    public function setClips(?int $clips = null): self
    {
        // validation for constraint: int
        if (!is_null($clips) && !(is_int($clips) || ctype_digit($clips))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($clips, true), gettype($clips)), __LINE__);
        }
        $this->clips = $clips;
        
        return $this;
    }
    /**
     * Get clipsLeft value
     * @return int|null
     */
    public function getClipsLeft(): ?int
    {
        return $this->clipsLeft;
    }
    /**
     * Set clipsLeft value
     * @param int $clipsLeft
     * @return \Exerp\Access\StructType\UsageInformation
     */
    public function setClipsLeft(?int $clipsLeft = null): self
    {
        // validation for constraint: int
        if (!is_null($clipsLeft) && !(is_int($clipsLeft) || ctype_digit($clipsLeft))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($clipsLeft, true), gettype($clipsLeft)), __LINE__);
        }
        $this->clipsLeft = $clipsLeft;
        
        return $this;
    }
    /**
     * Get expireDate value
     * @return string|null
     */
    public function getExpireDate(): ?string
    {
        return $this->expireDate;
    }
    /**
     * Set expireDate value
     * @param string $expireDate
     * @return \Exerp\Access\StructType\UsageInformation
     */
    public function setExpireDate(?string $expireDate = null): self
    {
        // validation for constraint: string
        if (!is_null($expireDate) && !is_string($expireDate)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($expireDate, true), gettype($expireDate)), __LINE__);
        }
        $this->expireDate = $expireDate;
        
        return $this;
    }
    /**
     * Get price value
     * @return string|null
     */
    public function getPrice(): ?string
    {
        return $this->price;
    }
    /**
     * Set price value
     * @param string $price
     * @return \Exerp\Access\StructType\UsageInformation
     */
    public function setPrice(?string $price = null): self
    {
        // validation for constraint: string
        if (!is_null($price) && !is_string($price)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($price, true), gettype($price)), __LINE__);
        }
        $this->price = $price;
        
        return $this;
    }
}
