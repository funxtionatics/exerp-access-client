<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for registerAttendByAccessCardParameters StructType
 * @subpackage Structs
 */
class RegisterAttendByAccessCardParameters extends RegisterAttendByAccessCardNoPrivilegeParameters
{
}
