<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for deactivateMemberCardResponse StructType
 * @subpackage Structs
 */
class DeactivateMemberCardResponse extends AbstractStructBase
{
    /**
     * The memberCard
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\MemberCardWithStatus[]
     */
    protected ?array $memberCard = null;
    /**
     * Constructor method for deactivateMemberCardResponse
     * @uses DeactivateMemberCardResponse::setMemberCard()
     * @param \Exerp\Access\StructType\MemberCardWithStatus[] $memberCard
     */
    public function __construct(?array $memberCard = null)
    {
        $this
            ->setMemberCard($memberCard);
    }
    /**
     * Get memberCard value
     * @return \Exerp\Access\StructType\MemberCardWithStatus[]
     */
    public function getMemberCard(): ?array
    {
        return $this->memberCard;
    }
    /**
     * This method is responsible for validating the values passed to the setMemberCard method
     * This method is willingly generated in order to preserve the one-line inline validation within the setMemberCard method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateMemberCardForArrayConstraintsFromSetMemberCard(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $deactivateMemberCardResponseMemberCardItem) {
            // validation for constraint: itemType
            if (!$deactivateMemberCardResponseMemberCardItem instanceof \Exerp\Access\StructType\MemberCardWithStatus) {
                $invalidValues[] = is_object($deactivateMemberCardResponseMemberCardItem) ? get_class($deactivateMemberCardResponseMemberCardItem) : sprintf('%s(%s)', gettype($deactivateMemberCardResponseMemberCardItem), var_export($deactivateMemberCardResponseMemberCardItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The memberCard property can only contain items of type \Exerp\Access\StructType\MemberCardWithStatus, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set memberCard value
     * @throws InvalidArgumentException
     * @param \Exerp\Access\StructType\MemberCardWithStatus[] $memberCard
     * @return \Exerp\Access\StructType\DeactivateMemberCardResponse
     */
    public function setMemberCard(?array $memberCard = null): self
    {
        // validation for constraint: array
        if ('' !== ($memberCardArrayErrorMessage = self::validateMemberCardForArrayConstraintsFromSetMemberCard($memberCard))) {
            throw new InvalidArgumentException($memberCardArrayErrorMessage, __LINE__);
        }
        $this->memberCard = $memberCard;
        
        return $this;
    }
    /**
     * Add item to memberCard value
     * @throws InvalidArgumentException
     * @param \Exerp\Access\StructType\MemberCardWithStatus $item
     * @return \Exerp\Access\StructType\DeactivateMemberCardResponse
     */
    public function addToMemberCard(\Exerp\Access\StructType\MemberCardWithStatus $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Access\StructType\MemberCardWithStatus) {
            throw new InvalidArgumentException(sprintf('The memberCard property can only contain items of type \Exerp\Access\StructType\MemberCardWithStatus, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->memberCard[] = $item;
        
        return $this;
    }
}
