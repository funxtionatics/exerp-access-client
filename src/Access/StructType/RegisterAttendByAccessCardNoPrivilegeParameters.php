<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for registerAttendByAccessCardNoPrivilegeParameters StructType
 * @subpackage Structs
 */
class RegisterAttendByAccessCardNoPrivilegeParameters extends AbstractStructBase
{
    /**
     * The cardId
     * @var string|null
     */
    protected ?string $cardId = null;
    /**
     * The cardType
     * @var string|null
     */
    protected ?string $cardType = null;
    /**
     * The centerId
     * @var int|null
     */
    protected ?int $centerId = null;
    /**
     * The date
     * @var string|null
     */
    protected ?string $date = null;
    /**
     * The resourceExternalId
     * @var string|null
     */
    protected ?string $resourceExternalId = null;
    /**
     * The startTime
     * @var string|null
     */
    protected ?string $startTime = null;
    /**
     * Constructor method for registerAttendByAccessCardNoPrivilegeParameters
     * @uses RegisterAttendByAccessCardNoPrivilegeParameters::setCardId()
     * @uses RegisterAttendByAccessCardNoPrivilegeParameters::setCardType()
     * @uses RegisterAttendByAccessCardNoPrivilegeParameters::setCenterId()
     * @uses RegisterAttendByAccessCardNoPrivilegeParameters::setDate()
     * @uses RegisterAttendByAccessCardNoPrivilegeParameters::setResourceExternalId()
     * @uses RegisterAttendByAccessCardNoPrivilegeParameters::setStartTime()
     * @param string $cardId
     * @param string $cardType
     * @param int $centerId
     * @param string $date
     * @param string $resourceExternalId
     * @param string $startTime
     */
    public function __construct(?string $cardId = null, ?string $cardType = null, ?int $centerId = null, ?string $date = null, ?string $resourceExternalId = null, ?string $startTime = null)
    {
        $this
            ->setCardId($cardId)
            ->setCardType($cardType)
            ->setCenterId($centerId)
            ->setDate($date)
            ->setResourceExternalId($resourceExternalId)
            ->setStartTime($startTime);
    }
    /**
     * Get cardId value
     * @return string|null
     */
    public function getCardId(): ?string
    {
        return $this->cardId;
    }
    /**
     * Set cardId value
     * @param string $cardId
     * @return \Exerp\Access\StructType\RegisterAttendByAccessCardNoPrivilegeParameters
     */
    public function setCardId(?string $cardId = null): self
    {
        // validation for constraint: string
        if (!is_null($cardId) && !is_string($cardId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cardId, true), gettype($cardId)), __LINE__);
        }
        $this->cardId = $cardId;
        
        return $this;
    }
    /**
     * Get cardType value
     * @return string|null
     */
    public function getCardType(): ?string
    {
        return $this->cardType;
    }
    /**
     * Set cardType value
     * @uses \Exerp\Access\EnumType\AccessCardType::valueIsValid()
     * @uses \Exerp\Access\EnumType\AccessCardType::getValidValues()
     * @throws InvalidArgumentException
     * @param string $cardType
     * @return \Exerp\Access\StructType\RegisterAttendByAccessCardNoPrivilegeParameters
     */
    public function setCardType(?string $cardType = null): self
    {
        // validation for constraint: enumeration
        if (!\Exerp\Access\EnumType\AccessCardType::valueIsValid($cardType)) {
            throw new InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \Exerp\Access\EnumType\AccessCardType', is_array($cardType) ? implode(', ', $cardType) : var_export($cardType, true), implode(', ', \Exerp\Access\EnumType\AccessCardType::getValidValues())), __LINE__);
        }
        $this->cardType = $cardType;
        
        return $this;
    }
    /**
     * Get centerId value
     * @return int|null
     */
    public function getCenterId(): ?int
    {
        return $this->centerId;
    }
    /**
     * Set centerId value
     * @param int $centerId
     * @return \Exerp\Access\StructType\RegisterAttendByAccessCardNoPrivilegeParameters
     */
    public function setCenterId(?int $centerId = null): self
    {
        // validation for constraint: int
        if (!is_null($centerId) && !(is_int($centerId) || ctype_digit($centerId))) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($centerId, true), gettype($centerId)), __LINE__);
        }
        $this->centerId = $centerId;
        
        return $this;
    }
    /**
     * Get date value
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }
    /**
     * Set date value
     * @param string $date
     * @return \Exerp\Access\StructType\RegisterAttendByAccessCardNoPrivilegeParameters
     */
    public function setDate(?string $date = null): self
    {
        // validation for constraint: string
        if (!is_null($date) && !is_string($date)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($date, true), gettype($date)), __LINE__);
        }
        $this->date = $date;
        
        return $this;
    }
    /**
     * Get resourceExternalId value
     * @return string|null
     */
    public function getResourceExternalId(): ?string
    {
        return $this->resourceExternalId;
    }
    /**
     * Set resourceExternalId value
     * @param string $resourceExternalId
     * @return \Exerp\Access\StructType\RegisterAttendByAccessCardNoPrivilegeParameters
     */
    public function setResourceExternalId(?string $resourceExternalId = null): self
    {
        // validation for constraint: string
        if (!is_null($resourceExternalId) && !is_string($resourceExternalId)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($resourceExternalId, true), gettype($resourceExternalId)), __LINE__);
        }
        $this->resourceExternalId = $resourceExternalId;
        
        return $this;
    }
    /**
     * Get startTime value
     * @return string|null
     */
    public function getStartTime(): ?string
    {
        return $this->startTime;
    }
    /**
     * Set startTime value
     * @param string $startTime
     * @return \Exerp\Access\StructType\RegisterAttendByAccessCardNoPrivilegeParameters
     */
    public function setStartTime(?string $startTime = null): self
    {
        // validation for constraint: string
        if (!is_null($startTime) && !is_string($startTime)) {
            throw new InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startTime, true), gettype($startTime)), __LINE__);
        }
        $this->startTime = $startTime;
        
        return $this;
    }
}
