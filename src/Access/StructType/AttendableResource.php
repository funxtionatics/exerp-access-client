<?php

declare(strict_types=1);

namespace Exerp\Access\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for attendableResource StructType
 * @subpackage Structs
 */
class AttendableResource extends AbstractStructBase
{
    /**
     * The resource
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\_resource|null
     */
    protected ?\Exerp\Access\StructType\_resource $resource = null;
    /**
     * The usageInformation
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \Exerp\Access\StructType\UsageInformation|null
     */
    protected ?\Exerp\Access\StructType\UsageInformation $usageInformation = null;
    /**
     * Constructor method for attendableResource
     * @uses AttendableResource::setResource()
     * @uses AttendableResource::setUsageInformation()
     * @param \Exerp\Access\StructType\_resource $resource
     * @param \Exerp\Access\StructType\UsageInformation $usageInformation
     */
    public function __construct(?\Exerp\Access\StructType\_resource $resource = null, ?\Exerp\Access\StructType\UsageInformation $usageInformation = null)
    {
        $this
            ->setResource($resource)
            ->setUsageInformation($usageInformation);
    }
    /**
     * Get resource value
     * @return \Exerp\Access\StructType\_resource|null
     */
    public function getResource(): ?\Exerp\Access\StructType\_resource
    {
        return $this->resource;
    }
    /**
     * Set resource value
     * @param \Exerp\Access\StructType\_resource $resource
     * @return \Exerp\Access\StructType\AttendableResource
     */
    public function setResource(?\Exerp\Access\StructType\_resource $resource = null): self
    {
        $this->resource = $resource;
        
        return $this;
    }
    /**
     * Get usageInformation value
     * @return \Exerp\Access\StructType\UsageInformation|null
     */
    public function getUsageInformation(): ?\Exerp\Access\StructType\UsageInformation
    {
        return $this->usageInformation;
    }
    /**
     * Set usageInformation value
     * @param \Exerp\Access\StructType\UsageInformation $usageInformation
     * @return \Exerp\Access\StructType\AttendableResource
     */
    public function setUsageInformation(?\Exerp\Access\StructType\UsageInformation $usageInformation = null): self
    {
        $this->usageInformation = $usageInformation;
        
        return $this;
    }
}
