<?php

declare(strict_types=1);

namespace Exerp\Access\ArrayType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for centerPresenceArray ArrayType
 * Meta information extracted from the WSDL
 * - final: #all
 * @subpackage Arrays
 */
class CenterPresenceArray extends AbstractStructArrayBase
{
    /**
     * The item
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \Exerp\Access\StructType\CenterPresence[]
     */
    protected ?array $item = null;
    /**
     * Constructor method for centerPresenceArray
     * @uses CenterPresenceArray::setItem()
     * @param \Exerp\Access\StructType\CenterPresence[] $item
     */
    public function __construct(?array $item = null)
    {
        $this
            ->setItem($item);
    }
    /**
     * Get item value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \Exerp\Access\StructType\CenterPresence[]
     */
    public function getItem(): ?array
    {
        return isset($this->item) ? $this->item : null;
    }
    /**
     * This method is responsible for validating the values passed to the setItem method
     * This method is willingly generated in order to preserve the one-line inline validation within the setItem method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateItemForArrayConstraintsFromSetItem(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message = '';
        $invalidValues = [];
        foreach ($values as $centerPresenceArrayItemItem) {
            // validation for constraint: itemType
            if (!$centerPresenceArrayItemItem instanceof \Exerp\Access\StructType\CenterPresence) {
                $invalidValues[] = is_object($centerPresenceArrayItemItem) ? get_class($centerPresenceArrayItemItem) : sprintf('%s(%s)', gettype($centerPresenceArrayItemItem), var_export($centerPresenceArrayItemItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The item property can only contain items of type \Exerp\Access\StructType\CenterPresence, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        
        return $message;
    }
    /**
     * Set item value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws InvalidArgumentException
     * @param \Exerp\Access\StructType\CenterPresence[] $item
     * @return \Exerp\Access\ArrayType\CenterPresenceArray
     */
    public function setItem(?array $item = null): self
    {
        // validation for constraint: array
        if ('' !== ($itemArrayErrorMessage = self::validateItemForArrayConstraintsFromSetItem($item))) {
            throw new InvalidArgumentException($itemArrayErrorMessage, __LINE__);
        }
        if (is_null($item) || (is_array($item) && empty($item))) {
            unset($this->item);
        } else {
            $this->item = $item;
        }
        
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \Exerp\Access\StructType\CenterPresence|null
     */
    public function current(): ?\Exerp\Access\StructType\CenterPresence
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \Exerp\Access\StructType\CenterPresence|null
     */
    public function item($index): ?\Exerp\Access\StructType\CenterPresence
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \Exerp\Access\StructType\CenterPresence|null
     */
    public function first(): ?\Exerp\Access\StructType\CenterPresence
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \Exerp\Access\StructType\CenterPresence|null
     */
    public function last(): ?\Exerp\Access\StructType\CenterPresence
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \Exerp\Access\StructType\CenterPresence|null
     */
    public function offsetGet($offset): ?\Exerp\Access\StructType\CenterPresence
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws InvalidArgumentException
     * @param \Exerp\Access\StructType\CenterPresence $item
     * @return \Exerp\Access\ArrayType\CenterPresenceArray
     */
    public function add($item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof \Exerp\Access\StructType\CenterPresence) {
            throw new InvalidArgumentException(sprintf('The item property can only contain items of type \Exerp\Access\StructType\CenterPresence, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string item
     */
    public function getAttributeName(): string
    {
        return 'item';
    }
}
