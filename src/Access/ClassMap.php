<?php

declare(strict_types=1);

namespace Exerp\Access;

/**
 * Class which returns the class map definition
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get(): array
    {
        return [
            'registerAttendByAccessCardNoPrivilegeParameters' => '\\Exerp\\Access\\StructType\\RegisterAttendByAccessCardNoPrivilegeParameters',
            'errorDetail' => '\\Exerp\\Access\\StructType\\ErrorDetail',
            'apiPersonKey' => '\\Exerp\\Access\\StructType\\ApiPersonKey',
            'compositeKey' => '\\Exerp\\Access\\StructType\\CompositeKey',
            'getTemporaryQRCodeParameters' => '\\Exerp\\Access\\StructType\\GetTemporaryQRCodeParameters',
            'resourcesAvailableForAttendsParameters' => '\\Exerp\\Access\\StructType\\ResourcesAvailableForAttendsParameters',
            'attendableResource' => '\\Exerp\\Access\\StructType\\AttendableResource',
            'resource' => '\\Exerp\\Access\\StructType\\_resource',
            'seats' => '\\Exerp\\Access\\StructType\\Seats',
            'seat' => '\\Exerp\\Access\\StructType\\Seat',
            'usageInformation' => '\\Exerp\\Access\\StructType\\UsageInformation',
            'registerAttendByAccessCardParameters' => '\\Exerp\\Access\\StructType\\RegisterAttendByAccessCardParameters',
            'accessGateParameters' => '\\Exerp\\Access\\StructType\\AccessGateParameters',
            'memberCard' => '\\Exerp\\Access\\StructType\\MemberCard',
            'apiUsagePointSourceKey' => '\\Exerp\\Access\\StructType\\ApiUsagePointSourceKey',
            'accessGateResult' => '\\Exerp\\Access\\StructType\\AccessGateResult',
            'gateCommandData' => '\\Exerp\\Access\\StructType\\GateCommandData',
            'personSimple' => '\\Exerp\\Access\\StructType\\PersonSimple',
            'personName' => '\\Exerp\\Access\\StructType\\PersonName',
            'assignCardParameter' => '\\Exerp\\Access\\StructType\\AssignCardParameter',
            'getUsagePointSourceResponse' => '\\Exerp\\Access\\StructType\\GetUsagePointSourceResponse',
            'usagePointSources' => '\\Exerp\\Access\\StructType\\UsagePointSources',
            'usagePointSource' => '\\Exerp\\Access\\StructType\\UsagePointSource',
            'federatedIntegerKey' => '\\Exerp\\Access\\StructType\\FederatedIntegerKey',
            'deactivateMemberCardParameters' => '\\Exerp\\Access\\StructType\\DeactivateMemberCardParameters',
            'deactivateMemberCardResponse' => '\\Exerp\\Access\\StructType\\DeactivateMemberCardResponse',
            'memberCardWithStatus' => '\\Exerp\\Access\\StructType\\MemberCardWithStatus',
            'centerPresence' => '\\Exerp\\Access\\StructType\\CenterPresence',
            'attendableResourceArray' => '\\Exerp\\Access\\ArrayType\\AttendableResourceArray',
            'centerPresenceArray' => '\\Exerp\\Access\\ArrayType\\CenterPresenceArray',
            'registerAttendByAccessCardNoPrivilegeParametersArray' => '\\Exerp\\Access\\ArrayType\\RegisterAttendByAccessCardNoPrivilegeParametersArray',
            'APIException' => '\\Exerp\\Access\\StructType\\APIException',
            'stringArray' => '\\Exerp\\Access\\ArrayType\\StringArray',
        ];
    }
}
