<?php

declare(strict_types=1);

namespace Exerp\Access\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Get ServiceType
 * @subpackage Services
 */
class Get extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named getTemporaryQRCode
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Access\StructType\GetTemporaryQRCodeParameters $parameters
     * @return string|bool
     */
    public function getTemporaryQRCode(\Exerp\Access\StructType\GetTemporaryQRCodeParameters $parameters)
    {
        try {
            $this->setResult($resultGetTemporaryQRCode = $this->getSoapClient()->__soapCall('getTemporaryQRCode', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetTemporaryQRCode;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getAttendableResources
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Access\StructType\ResourcesAvailableForAttendsParameters $parameters
     * @return \Exerp\Access\ArrayType\AttendableResourceArray|bool
     */
    public function getAttendableResources(\Exerp\Access\StructType\ResourcesAvailableForAttendsParameters $parameters)
    {
        try {
            $this->setResult($resultGetAttendableResources = $this->getSoapClient()->__soapCall('getAttendableResources', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultGetAttendableResources;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getUsageSource
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $centerId
     * @return \Exerp\Access\StructType\GetUsagePointSourceResponse|bool
     */
    public function getUsageSource($centerId)
    {
        try {
            $this->setResult($resultGetUsageSource = $this->getSoapClient()->__soapCall('getUsageSource', [
                $centerId,
            ], [], [], $this->outputHeaders));
        
            return $resultGetUsageSource;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named getPersonAttends
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     * @param string $fromDate
     * @param string $toDate
     * @param string $attends
     * @param string $checkIns
     * @return \Exerp\Access\ArrayType\CenterPresenceArray|bool
     */
    public function getPersonAttends(\Exerp\Access\StructType\ApiPersonKey $personKey, $fromDate, $toDate, $attends, $checkIns)
    {
        try {
            $this->setResult($resultGetPersonAttends = $this->getSoapClient()->__soapCall('getPersonAttends', [
                $personKey,
                $fromDate,
                $toDate,
                $attends,
                $checkIns,
            ], [], [], $this->outputHeaders));
        
            return $resultGetPersonAttends;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return string|\Exerp\Access\ArrayType\AttendableResourceArray|\Exerp\Access\ArrayType\CenterPresenceArray|\Exerp\Access\StructType\GetUsagePointSourceResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
