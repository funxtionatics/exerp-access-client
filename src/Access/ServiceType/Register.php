<?php

declare(strict_types=1);

namespace Exerp\Access\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Register ServiceType
 * @subpackage Services
 */
class Register extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named
     * registerAttendByAccessCardNoPrivilege
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Access\StructType\RegisterAttendByAccessCardNoPrivilegeParameters $parameters
     * @return void|bool
     */
    public function registerAttendByAccessCardNoPrivilege(\Exerp\Access\StructType\RegisterAttendByAccessCardNoPrivilegeParameters $parameters)
    {
        try {
            $this->setResult($resultRegisterAttendByAccessCardNoPrivilege = $this->getSoapClient()->__soapCall('registerAttendByAccessCardNoPrivilege', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultRegisterAttendByAccessCardNoPrivilege;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named registerAttendByAccessCard
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Access\StructType\RegisterAttendByAccessCardParameters $parameters
     * @return void|bool
     */
    public function registerAttendByAccessCard(\Exerp\Access\StructType\RegisterAttendByAccessCardParameters $parameters)
    {
        try {
            $this->setResult($resultRegisterAttendByAccessCard = $this->getSoapClient()->__soapCall('registerAttendByAccessCard', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultRegisterAttendByAccessCard;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named registerAttendNoPrivilege
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Access\StructType\CompositeKey $roomResourceKey
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     * @param string $date
     * @param string $startTime
     * @return void|bool
     */
    public function registerAttendNoPrivilege(\Exerp\Access\StructType\CompositeKey $roomResourceKey, \Exerp\Access\StructType\ApiPersonKey $personKey, $date, $startTime)
    {
        try {
            $this->setResult($resultRegisterAttendNoPrivilege = $this->getSoapClient()->__soapCall('registerAttendNoPrivilege', [
                $roomResourceKey,
                $personKey,
                $date,
                $startTime,
            ], [], [], $this->outputHeaders));
        
            return $resultRegisterAttendNoPrivilege;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named
     * registerMultipleAttendByAccessCardNoPrivilege
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Access\ArrayType\RegisterAttendByAccessCardNoPrivilegeParametersArray $attends
     * @return \Exerp\Access\ArrayType\StringArray|bool
     */
    public function registerMultipleAttendByAccessCardNoPrivilege(\Exerp\Access\ArrayType\RegisterAttendByAccessCardNoPrivilegeParametersArray $attends)
    {
        try {
            $this->setResult($resultRegisterMultipleAttendByAccessCardNoPrivilege = $this->getSoapClient()->__soapCall('registerMultipleAttendByAccessCardNoPrivilege', [
                $attends,
            ], [], [], $this->outputHeaders));
        
            return $resultRegisterMultipleAttendByAccessCardNoPrivilege;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return void|\Exerp\Access\ArrayType\StringArray
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
