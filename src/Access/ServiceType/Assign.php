<?php

declare(strict_types=1);

namespace Exerp\Access\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Assign ServiceType
 * @subpackage Services
 */
class Assign extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named assignCard
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Access\StructType\AssignCardParameter $parameters
     * @return void|bool
     */
    public function assignCard(\Exerp\Access\StructType\AssignCardParameter $parameters)
    {
        try {
            $this->setResult($resultAssignCard = $this->getSoapClient()->__soapCall('assignCard', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultAssignCard;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return void
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
