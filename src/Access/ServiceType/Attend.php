<?php

declare(strict_types=1);

namespace Exerp\Access\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Attend ServiceType
 * @subpackage Services
 */
class Attend extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named attendResource
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Access\StructType\CompositeKey $roomResourceKey
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     * @return void|bool
     */
    public function attendResource(\Exerp\Access\StructType\CompositeKey $roomResourceKey, \Exerp\Access\StructType\ApiPersonKey $personKey)
    {
        try {
            $this->setResult($resultAttendResource = $this->getSoapClient()->__soapCall('attendResource', [
                $roomResourceKey,
                $personKey,
            ], [], [], $this->outputHeaders));
        
            return $resultAttendResource;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return void
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
