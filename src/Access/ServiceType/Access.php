<?php

declare(strict_types=1);

namespace Exerp\Access\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Access ServiceType
 * @subpackage Services
 */
class Access extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named accessGate
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Access\StructType\AccessGateParameters $parameters
     * @return \Exerp\Access\StructType\AccessGateResult|bool
     */
    public function accessGate(\Exerp\Access\StructType\AccessGateParameters $parameters)
    {
        try {
            $this->setResult($resultAccessGate = $this->getSoapClient()->__soapCall('accessGate', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultAccessGate;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Exerp\Access\StructType\AccessGateResult
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
