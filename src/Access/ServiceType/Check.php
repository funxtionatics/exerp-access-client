<?php

declare(strict_types=1);

namespace Exerp\Access\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Check ServiceType
 * @subpackage Services
 */
class Check extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named checkInPerson
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param string $center
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     * @return boolean|bool
     */
    public function checkInPerson($center, \Exerp\Access\StructType\ApiPersonKey $personKey)
    {
        try {
            $this->setResult($resultCheckInPerson = $this->getSoapClient()->__soapCall('checkInPerson', [
                $center,
                $personKey,
            ], [], [], $this->outputHeaders));
        
            return $resultCheckInPerson;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Method to call the operation originally named checkAccess
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Access\StructType\CompositeKey $roomResourceKey
     * @param \Exerp\Access\StructType\ApiPersonKey $personKey
     * @return boolean|bool
     */
    public function checkAccess(\Exerp\Access\StructType\CompositeKey $roomResourceKey, \Exerp\Access\StructType\ApiPersonKey $personKey)
    {
        try {
            $this->setResult($resultCheckAccess = $this->getSoapClient()->__soapCall('checkAccess', [
                $roomResourceKey,
                $personKey,
            ], [], [], $this->outputHeaders));
        
            return $resultCheckAccess;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return boolean
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
