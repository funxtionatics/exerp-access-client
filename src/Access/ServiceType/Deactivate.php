<?php

declare(strict_types=1);

namespace Exerp\Access\ServiceType;

use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Deactivate ServiceType
 * @subpackage Services
 */
class Deactivate extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named deactivateMemberCard
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \Exerp\Access\StructType\DeactivateMemberCardParameters $parameters
     * @return \Exerp\Access\StructType\DeactivateMemberCardResponse|bool
     */
    public function deactivateMemberCard(\Exerp\Access\StructType\DeactivateMemberCardParameters $parameters)
    {
        try {
            $this->setResult($resultDeactivateMemberCard = $this->getSoapClient()->__soapCall('deactivateMemberCard', [
                $parameters,
            ], [], [], $this->outputHeaders));
        
            return $resultDeactivateMemberCard;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
        
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \Exerp\Access\StructType\DeactivateMemberCardResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
