<?php

declare(strict_types=1);

namespace Exerp\Access\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for centerPresenceType EnumType
 * @subpackage Enumerations
 */
class CenterPresenceType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'ATTEND'
     * @return string 'ATTEND'
     */
    const VALUE_ATTEND = 'ATTEND';
    /**
     * Constant for value 'CHECKIN'
     * @return string 'CHECKIN'
     */
    const VALUE_CHECKIN = 'CHECKIN';
    /**
     * Return allowed values
     * @uses self::VALUE_ATTEND
     * @uses self::VALUE_CHECKIN
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_ATTEND,
            self::VALUE_CHECKIN,
        ];
    }
}
