<?php

declare(strict_types=1);

namespace Exerp\Access\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for memberCardStatus EnumType
 * @subpackage Enumerations
 */
class MemberCardStatus extends AbstractStructEnumBase
{
    /**
     * Constant for value 'OK'
     * @return string 'OK'
     */
    const VALUE_OK = 'OK';
    /**
     * Constant for value 'STOLEN'
     * @return string 'STOLEN'
     */
    const VALUE_STOLEN = 'STOLEN';
    /**
     * Constant for value 'MISSING'
     * @return string 'MISSING'
     */
    const VALUE_MISSING = 'MISSING';
    /**
     * Constant for value 'BLOCKED'
     * @return string 'BLOCKED'
     */
    const VALUE_BLOCKED = 'BLOCKED';
    /**
     * Constant for value 'BROKEN'
     * @return string 'BROKEN'
     */
    const VALUE_BROKEN = 'BROKEN';
    /**
     * Constant for value 'RETURNED'
     * @return string 'RETURNED'
     */
    const VALUE_RETURNED = 'RETURNED';
    /**
     * Constant for value 'EXPIRED'
     * @return string 'EXPIRED'
     */
    const VALUE_EXPIRED = 'EXPIRED';
    /**
     * Constant for value 'DELETED'
     * @return string 'DELETED'
     */
    const VALUE_DELETED = 'DELETED';
    /**
     * Constant for value 'COMPROMISED'
     * @return string 'COMPROMISED'
     */
    const VALUE_COMPROMISED = 'COMPROMISED';
    /**
     * Constant for value 'FORGOTTEN'
     * @return string 'FORGOTTEN'
     */
    const VALUE_FORGOTTEN = 'FORGOTTEN';
    /**
     * Constant for value 'BANNED'
     * @return string 'BANNED'
     */
    const VALUE_BANNED = 'BANNED';
    /**
     * Return allowed values
     * @uses self::VALUE_OK
     * @uses self::VALUE_STOLEN
     * @uses self::VALUE_MISSING
     * @uses self::VALUE_BLOCKED
     * @uses self::VALUE_BROKEN
     * @uses self::VALUE_RETURNED
     * @uses self::VALUE_EXPIRED
     * @uses self::VALUE_DELETED
     * @uses self::VALUE_COMPROMISED
     * @uses self::VALUE_FORGOTTEN
     * @uses self::VALUE_BANNED
     * @return string[]
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_OK,
            self::VALUE_STOLEN,
            self::VALUE_MISSING,
            self::VALUE_BLOCKED,
            self::VALUE_BROKEN,
            self::VALUE_RETURNED,
            self::VALUE_EXPIRED,
            self::VALUE_DELETED,
            self::VALUE_COMPROMISED,
            self::VALUE_FORGOTTEN,
            self::VALUE_BANNED,
        ];
    }
}
