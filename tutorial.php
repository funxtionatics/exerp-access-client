<?php
/**
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the first needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientBase class (each generated ServiceType class extends this class)
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = [
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://goodlife-test.exerp.com/api/v5/AccessAPI?wsdl',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * ];
 * etc...
 */
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = [
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://goodlife-test.exerp.com/api/v5/AccessAPI?wsdl',
    WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \Exerp\Access\ClassMap::get(),
];
/**
 * Samples for Register ServiceType
 */
$register = new \Exerp\Access\ServiceType\Register($options);
/**
 * Sample call for registerAttendByAccessCardNoPrivilege operation/method
 */
if ($register->registerAttendByAccessCardNoPrivilege(new \Exerp\Access\StructType\RegisterAttendByAccessCardNoPrivilegeParameters()) !== false) {
    print_r($register->getResult());
} else {
    print_r($register->getLastError());
}
/**
 * Sample call for registerAttendByAccessCard operation/method
 */
if ($register->registerAttendByAccessCard(new \Exerp\Access\StructType\RegisterAttendByAccessCardParameters()) !== false) {
    print_r($register->getResult());
} else {
    print_r($register->getLastError());
}
/**
 * Sample call for registerAttendNoPrivilege operation/method
 */
if ($register->registerAttendNoPrivilege(new \Exerp\Access\StructType\CompositeKey(), new \Exerp\Access\StructType\ApiPersonKey(), $date, $startTime) !== false) {
    print_r($register->getResult());
} else {
    print_r($register->getLastError());
}
/**
 * Sample call for registerMultipleAttendByAccessCardNoPrivilege operation/method
 */
if ($register->registerMultipleAttendByAccessCardNoPrivilege(new \Exerp\Access\ArrayType\RegisterAttendByAccessCardNoPrivilegeParametersArray()) !== false) {
    print_r($register->getResult());
} else {
    print_r($register->getLastError());
}
/**
 * Samples for Check ServiceType
 */
$check = new \Exerp\Access\ServiceType\Check($options);
/**
 * Sample call for checkInPerson operation/method
 */
if ($check->checkInPerson($center, new \Exerp\Access\StructType\ApiPersonKey()) !== false) {
    print_r($check->getResult());
} else {
    print_r($check->getLastError());
}
/**
 * Sample call for checkAccess operation/method
 */
if ($check->checkAccess(new \Exerp\Access\StructType\CompositeKey(), new \Exerp\Access\StructType\ApiPersonKey()) !== false) {
    print_r($check->getResult());
} else {
    print_r($check->getLastError());
}
/**
 * Samples for Attend ServiceType
 */
$attend = new \Exerp\Access\ServiceType\Attend($options);
/**
 * Sample call for attendResource operation/method
 */
if ($attend->attendResource(new \Exerp\Access\StructType\CompositeKey(), new \Exerp\Access\StructType\ApiPersonKey()) !== false) {
    print_r($attend->getResult());
} else {
    print_r($attend->getLastError());
}
/**
 * Samples for Get ServiceType
 */
$get = new \Exerp\Access\ServiceType\Get($options);
/**
 * Sample call for getTemporaryQRCode operation/method
 */
if ($get->getTemporaryQRCode(new \Exerp\Access\StructType\GetTemporaryQRCodeParameters()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getAttendableResources operation/method
 */
if ($get->getAttendableResources(new \Exerp\Access\StructType\ResourcesAvailableForAttendsParameters()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getUsageSource operation/method
 */
if ($get->getUsageSource($centerId) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getPersonAttends operation/method
 */
if ($get->getPersonAttends(new \Exerp\Access\StructType\ApiPersonKey(), $fromDate, $toDate, $attends, $checkIns) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Samples for Access ServiceType
 */
$access = new \Exerp\Access\ServiceType\Access($options);
/**
 * Sample call for accessGate operation/method
 */
if ($access->accessGate(new \Exerp\Access\StructType\AccessGateParameters()) !== false) {
    print_r($access->getResult());
} else {
    print_r($access->getLastError());
}
/**
 * Samples for Assign ServiceType
 */
$assign = new \Exerp\Access\ServiceType\Assign($options);
/**
 * Sample call for assignCard operation/method
 */
if ($assign->assignCard(new \Exerp\Access\StructType\AssignCardParameter()) !== false) {
    print_r($assign->getResult());
} else {
    print_r($assign->getLastError());
}
/**
 * Samples for Deactivate ServiceType
 */
$deactivate = new \Exerp\Access\ServiceType\Deactivate($options);
/**
 * Sample call for deactivateMemberCard operation/method
 */
if ($deactivate->deactivateMemberCard(new \Exerp\Access\StructType\DeactivateMemberCardParameters()) !== false) {
    print_r($deactivate->getResult());
} else {
    print_r($deactivate->getLastError());
}
